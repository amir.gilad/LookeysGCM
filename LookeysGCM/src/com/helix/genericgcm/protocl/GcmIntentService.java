package com.helix.genericgcm.protocl;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.helix.genericgcm.concrete.GCMMessageHandler;
import com.helix.genericgcm.concrete.Parameters;
import com.helix.genericgcm.concrete.VersionUpdateThread;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;

/**
 * This {@code IntentService} does the actual handling of the GCM message.
 * {@code GcmBroadcastReceiver} (a {@code WakefulBroadcastReceiver}) holds a
 * partial wake lock for this service while the service does its work. When the
 * service is finished, it calls {@code completeWakefulIntent()} to release the
 * wake lock.
 */
public class GcmIntentService extends IntentService {
	public static final String TAG = "Lookeys GcmIntentService";
	public static final int NOTIFICATION_ID = 1;
	
    private GCMMessageHandler messageHandler;

    public GcmIntentService() {
    	
        super("GcmIntentService");
        Context context = this;
        messageHandler = new GCMMessageHandler(context);
    }    

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.handleMessage
        String messageType = gcm.getMessageType(intent);

        if ((extras != null) && !extras.isEmpty()) {  // has effect of unparcelling Bundle
            /*
             * Filter messages based on message type. Since it is likely that GCM will be
             * extended in the future with new message types, just ignore any message types you're
             * not interested in, or that you don't recognize.
             */
            if (GoogleCloudMessaging.MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                //sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_DELETED.equals(messageType)) {
                //sendNotification("Deleted messages on server: " + extras.toString());
            // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                //AK: TODO - add handling to the different messages as needed
                // Post notification of received message.
            	String from = extras.getString(Parameters.FROM_PARAM);
            	String msg = extras.getString(Parameters.MESSAGE_PARAM);
            	
            	if(Parameters.ENABLE_DEBUG)
            		Log.i(TAG, String.format("Received message from (%s): %s", from, msg));
            	else Log.i(Parameters.RELEASE_TAG, "Sys Msg 10");
            	
        		VersionUpdateThread updateThread = new VersionUpdateThread();
            	long ret = messageHandler.handleMessage(msg, updateThread);
            	if(ret == -2)
            		return;
            	if(Parameters.ENABLE_DEBUG){
            		 if(ret < 0)
            			ret = Parameters.D_LOOKEYS_POSTPONE_DOWNLOAD;
            		else ret = ret*60*1000;
            	} else {
            		if(ret < 0)
            			ret = Parameters.LOOKEYS_POSTPONE_DOWNLOAD;
            		
            		else ret = ret*24*60*60*1000;
            	}
            	
            	long pospone = Math.round(Math.random() * ret);
            	if(Parameters.ENABLE_DEBUG) 
            		Log.w(TAG, "ret = " + ret + "  pospone = " + pospone);
            	
				SharedPreferences prefs = this.getApplicationContext().getSharedPreferences(GCMLogic.class.getSimpleName(),
                        Context.MODE_PRIVATE);
        		SharedPreferences.Editor editor = prefs.edit();
       		
        		boolean gcmSleep = prefs.getBoolean(Parameters.LOOKEYS_GCM_SLEEP, false);
        		if((gcmSleep) && (pospone != 0)){
        			long wakeTime = prefs.getLong(Parameters.LOOKEYS_GCM_TIME, 0);
        			if(wakeTime < System.currentTimeMillis()){
        				pospone = 0;
        			} else {
        				pospone = wakeTime-System.currentTimeMillis();
        			}
        			if(Parameters.ENABLE_DEBUG) 
                		Log.w(TAG, "gcm was sleeping new pospone = " + pospone);
        		}
        		try{
        			if(pospone > 1000){
                		editor.putLong(Parameters.LOOKEYS_GCM_TIME, System.currentTimeMillis()+pospone);
                		editor.putString(Parameters.LOOKEYS_GCM_MSG, msg);
                		editor.putBoolean(Parameters.LOOKEYS_GCM_SLEEP, true);
                		editor.commit();
                		Thread.sleep(pospone);
        			}
        				
        		} catch (InterruptedException e1) {
        			
        		}
        		
        		ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
                NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
                if(!mWifi.isConnected()) {
                    // not connected using wifi, try again later
                	if(Parameters.ENABLE_DEBUG) 
                		Log.w(TAG, "Wifi not connected");
                	return;
                }
        		editor.putBoolean(Parameters.LOOKEYS_GCM_SLEEP, false);
        		editor.commit();

        		if(Parameters.ENABLE_DEBUG) 
            		Log.w(TAG, "updateThread.start");
            	updateThread.start();
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        //GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

}
