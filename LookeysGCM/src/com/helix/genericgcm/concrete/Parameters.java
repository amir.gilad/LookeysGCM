package com.helix.genericgcm.concrete;

public class Parameters {
	public static final boolean ENABLE_DEBUG = true;
	//AK: change to sender id (the project in app engine for example) 
    public static final String SENDER_ID = "756941404061";
	//AK: change to third party server url
    public static final String THIRD_PARTY_REGISTRATION_URL = "http://lookeysautoupdater.appspot.com/device/registerByVersion";
    public static final String THIRD_PARTY_INSTALL_COMPLETE_URL = "http://lookeysautoupdater.appspot.com/lookeys/registerInstallComplete";
    public static final String APP_ENGINE_URL = "http://lookeysautoupdater.appspot.com";
    public static final String UPDATE_SERVER_URL = APP_ENGINE_URL + "/lookeys/getDeviceVersion";
    
    //3rd party registration parameters:
    public static final String DEVICE_ID_PARAM = "id";
    public static final String REG_DEVICE_ID_PARAM = "deviceId";
    public static final String REGISTRATION_ID_PARAM = "regId";
    public static final String INSTALL_APP_NAME = "activity";
    public static final String REGISTRATION_VERSION_PARAM = "version";
    public static final String REGISTRATION_VERSION_DEFAULT = "3_0";
    // The device ID to send in case of null device ID
    public static final String NULL_DEVICE_ID = "0000000";
    //3rd party message parameters:
    public static final String FROM_PARAM = "from";
    public static final String MESSAGE_PARAM = "msg";
    //Program package name : 
    //AK: TODO - update this with the correct lookeys applications package name
    public static final String LOOKEYS_PACKAGE_NAME = "lookeys.softkeyboard.indic";
    public static final String LOOKEYS_KBD_NAME = LOOKEYS_PACKAGE_NAME + "/com.android.inputmethod.latin.LatinIME";
    public static final String LOCAL_TEMP_PATH = "/data/local/tmp";
    public static final String LOCAL_DOWNLOAD_PATH = "/sdcard";
    public static final String TEMP_FILE_NAME = "up.apk";
    
    public static final String INTSALL_APP_MESSAGE_IDENTIFIER = "zpcimedlsuby";
    public static final String DELETE_APP_MESSAGE_IDENTIFIER = "pwmrlcoemgldmsk";
    
    // used for download delay
    public static final long LOOKEYS_POSTPONE_DOWNLOAD = 48*60*60*1000;
    public static final long D_LOOKEYS_POSTPONE_DOWNLOAD = 3*60*1000;  // for debug
    public static final String LOOKEYS_GCM_TIME = "LookeyGCMTime";
    public static final String LOOKEYS_GCM_SLEEP = "LookeyGCMSleep";
    public static final String LOOKEYS_GCM_MSG = "LookeysGCMMsg";
    
    public static final String LOOKEYS_LAST_PULL_TIME = "LookeysLastPullTime";
    public static final long LOOKEYS_PULL_PERIOD = 2*7*24*60*60 + 4*60*60;  // 2 weeks
    public static final long D_LOOKEYS_PULL_PERIOD = 60*2; // for debug
    public static final String PREF_PULL_UPGRADE_TIME = "LookeysPullUpgradeTime";
    
    public static final String PREF_INSTALLED_APP_NAME = "LookeysInstalledAppName";
    public static final String PREF_INSTALLED_CLASS_NAME = "LookeysInstalledClassName";
    public static final String PREF_SHOULD_ACTIVE_INSTALLED = "LookeysShouldActivate";
    
    public static final String RELEASE_TAG = "Sys285";
}
